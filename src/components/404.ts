import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-404',
    template: `
        <div class="wcim__404Page flexbox flex-alignc flex-justifyc flex__direction-column">
            <img src="https://www.angular.cn/assets/images/support/angular-404.svg" />
            <div class="info align-c">
                <h1 class="no-toc" id="page-not-found">Resource Not Found</h1><p>We're sorry. The resource cannot be found.</p>
                <p><a class="wc__btn-default mar30" routerLink="/index">返回首页</a></p>
            </div>
        </div>
    `,
    styles: [`
        .wcim__404Page{height:100%;}.wcim__404Page img{margin:.5rem 0; height: 3rem;}.wcim__404Page .info h1{color:#1976d2;font-size:.36rem;font-weight:500;text-transform: uppercase;}.wcim__404Page .info p{font-size:.28rem;}
    `]
})
export class NotFoundComponent implements OnInit {
    constructor() { }
    ngOnInit(): void { }
}
