
import { Router, CanActivate } from '@angular/router'
declare var wcPop: any;
export class Auth implements CanActivate{
    constructor(private router: Router){}
    canActivate(){
        let that = this
        const token: boolean = window.sessionStorage.getItem('token') ? true : false
        if(!token){that.router.navigate(['/login']);
        }
        return token
    }
}